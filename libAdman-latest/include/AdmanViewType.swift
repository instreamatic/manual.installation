/// Copyright 2019 Instreamatic
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Affero General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU Affero General Public License for more details.
/// You should have received a copy of the GNU Affero General Public License
/// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import UIKit

if !AdmanViewType_h {
//#define AdmanViewType_h


enum AdmanViewElement : Int {
    case admanViewContainer = 1
    case admanViewBanner
    case admanViewClose
    case admanViewPlay
    case admanViewPause
    case admanViewRewind
    case admanViewRemainingTime
    case admanViewVoiceContainer
    case admanViewPositive
    case admanViewNegative
    case admanViewMicStatus
    case admanViewVoiceRequestProgress
}

class AdmanViewBase: NSObject {
    func getView() -> UIView? {
    }
}

class AdmanViewBaseViewController: AdmanViewBase {
    override func getView() -> UIViewController? {
    }

    init(_ view: UIViewController) {
    }
}

class AdmanViewBaseView: AdmanViewBase {
    override func getView() -> UIView? {
    }

    init(_ view: UIView) {
    }
}

class AdmanViewBaseButton: AdmanViewBase {
    override func getView() -> UIButton? {
    }

    init(_ view: UIButton) {
    }
}

class AdmanViewBaseLabel: AdmanViewBase {
    override func getView() -> UILabel? {
    }

    init(_ view: UILabel) {
    }
}

class AdmanViewBaseImageView: AdmanViewBase {
    override func getView() -> UIImageView? {
    }

    init(_ view: UIImageView) {
    }
}

class AdmanViewBind: NSObject {
    var view: AdmanViewBase?
    var type: AdmanViewElement!

    init(_ type: AdmanViewElement, withView view: AdmanViewBase?) {
    }
}

protocol IAdmanViewBundle: NSObjectProtocol {
    func contains(_ type: AdmanViewElement) -> Bool
    func get(_ type: AdmanViewElement) -> AdmanViewBase?
}
/*
@interface AdmanViewBundle: NSObject<IAdmanViewBundle>
- (_Null_unspecified id)init:(NSMutableArray<AdmanViewBind *> * _Nonnull)bindings;
@end
*/

}
