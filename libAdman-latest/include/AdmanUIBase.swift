/// Copyright 2019 Instreamatic
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Affero General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU Affero General Public License for more details.
/// You should have received a copy of the GNU Affero General Public License
/// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import Foundation
import UIKit

class AdmanUIBase: UIViewController {
    var rootVC: UIViewController?
    var microphoneStatus: UIView?
    var voiceRequestAnimation: UIImageView?
    var bannerView: UIImageView?
    var closeView: UIImageView?
    var playView: UIImageView?
    var rewindView: UIImageView?
    var remainingTime: UILabel?
    var intentLabel: UILabel?
    var positiveView: UIImageView?
    var negativeView: UIImageView?

    class func add(_ sel: Selector, for view: UIView, withTarget delegate: Any) {
    }

    class func loadImage(from base64: String) -> UIImage {
    }

    func initBaseView() {
    }

    func initBaseComponents() {
    }

    func initVoiceView() {
    }

    func initVoiceComponents() {
    }

    func update(_ ad: AdmanBanner) {
    }

    func show(_ rootVC: UIViewController) {
    }

    func hide() {
    }

    func rotateByOrientation() {
    }

    func rotateVertical() {
    }

    func rotateHorizontal() {
    }

    func showPauseButton(_ tapGestureRecognizer: UITapGestureRecognizer?) {
    }

    func showPlayButton(_ tapGestureRecognizer: UITapGestureRecognizer?) {
    }

    func admanPlayerNotification(_ notification: Notification) {
    }

    func admanVoiceNotification(_ notification: Notification) {
    }
}
