/// Copyright 2021 Instreamatic
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Affero General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU Affero General Public License for more details.
/// You should have received a copy of the GNU Affero General Public License
/// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import AVFoundation
import UIKit

enum AdmanSectionType : Int {
    case sPreroll = 0
    case sMidroll
    case sPostroll
}

@objc protocol AdmanDelegate: NSObjectProtocol {
    /// Method called when adman state changed
    /// - Parameter sender: Adman instance
    func admanStateDidChange(_ sender: Adman)

    /// Only for voice recognition.
    /// - Parameter result: recognized word or phrase, empty string if not
    @objc optional func phraseRecognized(_ result: [AnyHashable : Any])
    @objc optional func customVoiceIntentHandler()

    /// Triggered when image tapped by user
    /// - Parameter urlToNavigate: url, opened in internal browser
    @objc optional func bannerTouched(_ urlToNavigate: String?)
    /// Triggered when default ad view(with control playback buttons) closed
    @objc optional func viewClosed()
    @objc optional func errorReceived(_ error: Error?)
    /// Triggered when dtmf label recived
    @objc optional func dtmfIn()
    @objc optional func dtmfOut()
    /// Triggered only for external debug
    @objc optional func log(_ message: String)
}

enum AdmanState : Int {
        /// default adman state
        case initial = 0
        /// preloading and parse creative from vast/json source
        case fetchingInfo
        /// ad can be played
        case readyForPlayback
        /// If creative contain more than one ad, ad changed when playback completed
        case adChangedForPlayback
        case playing
        case paused
        case playbackCompleted
        case stopped
        /// only for voice ads
        case voiceResponsePlaying
        case voiceInteractionStarted
        case voiceInteractionEnded
        /// error occurred during loading/playback, 11
        case error
        case typeMismatch
        case preloading
        case speechRecognizerStarted
        case speechRecognizerStopped
        /// no ads for this siteId, region, audio format or other params
        case adNone
    }

enum AdmanBannerType : Int {
        case t320x480 = 0
        case t320x50
    }

    /// @property state current Adman state
class Adman: NSObject, UIGestureRecognizerDelegate, NSXMLParserDelegate {
    var state: AdmanState!
    var fallbackState: AdmanState!
    private(set) var error: Error?
    weak var delegate: AdmanDelegate?
    /// @property adsList<AdmanBanner*> creatives list for show/playback on device
    var adsList: [AdmanBanner]?
    /// index of active ad in adsList
    private(set) var activeAd = 0
    /// @property mute mute/unmute playback for current audio ad
    var mute = false
    /// @property volume audio ad volume
    var volume: Float = 0.0
    /// @property elapsedTime time played in seconds from audio start
    private(set) var elapsedTime = 0
    /// @property elapsedTimeString time played in seconds as string from audio start
    private(set) var elapsedTimeString: String?
    private(set) var estimatedTime = 0
    private(set) var estimatedTimeString: String?
    var progress: Float = 0.0
    var userId: String?
    var playerId = 0
    private(set) var transcriptedPhrase: String?

    /// Initialize and return new Adman object
    class func sharedManager() -> Self {
    }

    /// - Parameter siteId: unique partner identifier, obtained from the Instreamatic manager
    class func sharedManager(withSiteId siteId: Int) -> Self {
    }

    /// - Parameter testMode: ignore all targeting params
    class func sharedManager(withSiteId siteId: Int, testMode: Bool) -> Self {
    }

    class func sharedManager(withSiteId siteId: Int, playerId: Int, testMode: Bool) -> Self {
    }

    class func sharedManager(withSiteId siteId: Int, region: AdmanRegion, testMode: Bool) -> Self {
    }

    class func sharedManager(withSiteId siteId: Int, region: AdmanRegion, playerId: Int, testMode: Bool) -> Self {
    }

    class func sharedManager(withSiteId siteId: Int, withZoneId zoneId: Int, testMode: Bool) -> Self {
    }

    init(url: String) {
    }

    init(siteId: Int, testMode: Bool, withZoneId zoneId: Int, region: AdmanRegion, playerId: Int) {
    }

    /// Send request to server for creative.
    /// When response will be parsed and audio ad loaded, Adman state changed to ReadyForPlayback
    /// Adman state may be AdmanStateAdNone if server return empty response
    func prepare() {
    }

    func prepare(with format: AdmanFormat) {
    }

    func prepare(with type: AdmanType) {
    }

    func prepare(_ format: AdmanFormat, type: AdmanType) {
    }

    /// - Parameters:
    ///   - maxDuration: limit in seconds summary ads playback duration. Set 0 for default value, but cannot be lower than 10.
    ///   - adsCount: limit maximum ads count requested from server, 1 by default.
    func prepare(withAdLimits maxDuration: Int, adsCount: Int) {
    }

    func prepare(_ format: AdmanFormat, type: AdmanType, maxDuration: Int, adsCount: Int) {
    }

    /// Check expiration time for ads list. If ads expired they will be preloaded automatically
    func isValidToPlayback() -> Bool {
    }

    /// Check if app already in background
    func `is`(inBackground synced: Bool, then cb: AdmanOnFulfill?) {
    }

    /// Check mirophone availabillity
    func isMicrophoneEnabled() -> Bool {
    }

    /// Cancel ad loading and audio buffering
    func cancelLoading() {
    }

    /// Start playback, Adman state changed to AdmanStatePlaying
    func play() {
    }

    /// Pause playback, state changed to AdmanStatePaused
    func pause() {
    }

    /// Resume playback, state changed to AdmanStatePlaying
    /// Playback cannot be resumed after [adman stop] method
    func resume() {
    }

    /// Stop playback and rewind to start.
    /// State changed to AdmanStatePlaying
    func rewind() {
    }

    /// Stop playback.
    /// State changed to AdmanStateStopped. Playback cannot be resumed.
    func stop() {
    }

    /// Place banner image in uiview
    /// - Parameters:
    ///   - spot: image view to place in
    ///   - type: image size
    func showBanner(forSpot spot: UIView, with type: AdmanBannerType) {
    }

    /// Report ad event
    /// - Parameter eventName: VAST-specific event name (see https://www.iab.com/guidelines/digital-video-ad-serving-template-vast-3-0 for more information about ad event names)
    /// first eventName letter should be always in lowerCase (e.g creativeView, clickTracking)
    func reportAdEvent(_ eventName: String) {
    }

    /// Return active Adman creative
    func getActiveAd() -> AdmanBanner? {
    }

    /// Enable dtmf ads detection for station name
    /// - Parameters:
    ///   - name: radiostation name
    ///   - preload: ad aotimaticaly after short delay
    ///   - vc: ViewController for default sdk banner view to display in
    func enableDtmfAdsDetection(forStation name: String, preload: Bool, vc: UIViewController?) {
    }

    func disableDtmfAdsDetection() {
    }

    /// Override default banner size on preloading resources phase if creative does not contain 640x640 image
    /// - Parameter size: banner WxH, where W - width, H - size
    func setDefaultBannerSize(_ size: String) {
    }

    func getDefaultBannerSize() -> String {
    }

    /// - Parameters:
    ///   - format: any supported on Apple devices media format (e.g. mp3, aac, alac)
    ///   - bitrate: only float or integer (e.g. 128000, 128, 128.419)
    func setDefaultAudioFormat(_ format: String, withBitrate bitrate: NSNumber?) {
    }

    func setServerVoiceActivityDetection(_ state: Bool) {
    }

    class func setPreloadMode(_ mode: AdmanPreloadMode) {
    }

    /// Set custom user ip if needed (advanced targeting)
    func overrideIP(_ ip: String) {
    }

    /// Set default voice recognition backend
    func setRecognitionBackend(_ backend: AdmanRecognitionBackend) {
    }

    /// Return current region
    func currentRegion() -> AdmanRegion {
    }

    /// Return instance siteId
    func siteId() -> Int {
    }

    /// Return current libAdman version in format "X.Y.Z"
    class func getVersion() -> String {
    }

    /// Return Adman ad server for user Region
    func getServer(_ region: AdmanRegion) -> String {
    }
}

extension Adman {
    /// ##### Common setup methods #####
    func campaignId() -> Int {
    }

    func setCampaignId(_ campaignId: Int) {
    }

    func creativeId() -> Int {
    }

    func setCreativeId(_ creativeId: Int) {
    }

    func setRecognitionBackendLocation(_ location: AdmanRecognitionBackendLocation) {
    }

    func recognitionBackendLocation() -> AdmanRecognitionBackendLocation {
    }

    /// 
    func enableGeolocationTargeting() {
    }

    /// Disable sdk AVAudioSession setup if app need to keep settings
    func disableAudioSessionSetup() {
    }

    /// Enable/disable debug mode
    func setPreview(_ mode: AdmanDebugMode) {
    }

    /// Enable preroll/midroll/postroll targeting
    func setAdSlot(_ slot: AdmanAdSlot) {
    }

    func getAdSlot() -> AdmanAdSlot {
    }

    func setCustomRegionServer(_ adServer: String) {
    }

    /// Set voice response delay
    func setResponseDelay(_ delay: Float) {
    }

    func getResponseDelay() -> Float {
    }

    func setResponseTime(_ duration: Float) {
    }

    func getResponseTime() -> Float {
    }

    /// Set max lenght for server waiting timeout
    func setServerTimeout(_ delay: Int) {
    }

    /// Set custom siteVariable to all ad requests
    func setSiteVariable(_ key: String, value: String) {
    }
}
