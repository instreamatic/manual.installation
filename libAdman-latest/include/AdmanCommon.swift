/// Copyright 2021 Instreamatic
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Affero General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU Affero General Public License for more details.
/// You should have received a copy of the GNU Affero General Public License
/// along with this program.  If not, see <https://www.gnu.org/licenses/>.

enum AdmanDebugMode : Int {
    /// test creative with image and audio
    case admanDebugBanner = 11
    /// test creative with audio
    case admanDebugAudio = 12
    /// test voice creative
    case admanDebugVoiceAd = 41
    /// debug mode disabled
    case admanDebugNone = 0
}

enum AdmanType : Int {
    case any = 1
    case audioOnly = 7
    case audioPlus = 6
    case voice = 12
    case voiceMultipleAds
}

enum AdmanRegion : Int {
    case eu = 0
    case us
    case global
    case demo
    case demoEu
    case voice
    case india
    case custom
}

enum AdmanFormat : Int {
    case any = 1
    case swiftSponsor = 2
    case gameVibe = 3
}

enum AdmanAdSlot : Int {
    case admanPreroll
    case admanMidroll
    case admanPostroll
    case `default`
}

enum AdmanRecognitionBackend : Int {
    case auto = 0
    case yandex
    case microsoft
    case nuanceDev
    case houndify
    case nuanceRelease
}

let AdmanStatEventLoad = "load"
let AdmanStatEventRequest = "request"
let AdmanStatEventFetched = "fetched"

let AdmanStatEventClickTracking = "clickTracking"
let AdmanStatEventCreativeView = "creativeView"
let AdmanStatEventImpression = "impression"
let AdmanStatEventCanShow = "can_show"
let AdmanStatEventStart = "start"
let AdmanStatEventFirstQuartile = "firstQuartile"
let AdmanStatEventMidpoint = "midpoint"
let AdmanStatEventThirdQuartile = "thirdQuartile"
let AdmanStatEventComplete = "complete"
let AdmanStatEventPause = "pause"
let AdmanStatEventRewind = "rewind"
let AdmanStatEventResume = "resume"
let AdmanStatEventMute = "mute"
let AdmanStatEventUnmute = "unmute"
let AdmanStatEventClose = "close"
let AdmanStatEventError = "error"

let AdmanStatEventSkip = "skip"
let AdmanStatEventResponseNegative = "respose_negative"
let AdmanStatEventResponseSilence = "respose_silence"
let AdmanStatEventResponseUnknown = "respose_unknown"
let AdmanStatEventResponseOpen = "respose_open"
let AdmanStatEventResponseSkip = "respose_skip"
let AdmanStatEventResponseSwearing = "respose_swearing"
let AdmanStatEventResponseError = "respose_error"
let AdmanStatEventResponseOpenBanner = "response_open_banner"
let AdmanStatEventResponseOpenButton = "response_open_button"
let AdmanStatEventResponseNegativeButton = "response_negative_button"

/// geo location for voice recognition server
/// Auto - default value, replaced by VAST responseURL section if present.
/// EU
/// USA
/// Demo - USA demo server
enum AdmanRecognitionBackendLocation : Int {
    case auto
    case eu
    case usa
    case demo
}

class AdmanCreative: NSObject {
    /// @property statistics<NSString*, NSArray*<NSString*>> stat data from Creative tracking
    var tracking: [String : [String]] = [:]
    var staticResource = ""
    /// @property urlToNavigateOnClick link to open in internal browser
    var urlToNavigateOnClick: String?
}

class AdmanStatRequest: NSObject {
    var isReported = false
    var onComplete: (() -> Void)?
    var onFail: ((Error?) -> Void)?

    init(fromArray URIs: [AnyHashable]) {
    }

    func addURL(_ URL: String) {
    }

    func report() {
    }
}

class AdmanStats: NSObject {
    var badRequests: [Date] = []

    override func value(forKey key: String) -> Any? {
    }

    func save(_ url: String, forEvent event: String) {
    }

    func save(
        for event: String,
        point: String,
        admanId: Int,
        siteId: Int,
        playerId: Int,
        campaignId: String?,
        bannerId: Int,
        deviceId: String?
    ) {
    }

    func reportEvent(_ eventName: String) {
    }

    func reportEvent(_ event: String, forCompanion companion: AdmanCreative?) {
    }

    func setRegion(_ region: AdmanRegion) {
    }

    func getServer() -> String {
    }
}
