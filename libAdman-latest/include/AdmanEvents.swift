/// Copyright 2019 Instreamatic
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Affero General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU Affero General Public License for more details.
/// You should have received a copy of the GNU Affero General Public License
/// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import UIKit

if !AdmanEvents_h {
//#define AdmanEvents_h



let NOTIFICATION_ADMAN_MESSAGE = "NotificationMessageEventAdman"
let NOTIFICATION_ADMAN_MESSAGE_KEY = "adman_message"
enum AdmanMessageEventType : Int {
    case admanMessageEventFetchingInfo = 1
    case admanMessageEventPreloading
    case admanMessageEventNone
    case admanMessageEventError
    case admanMessageEventReady
    case admanMessageEventStarted
    case admanMessageEventStopped
    case admanMessageEventAlmostCompleted
    case admanMessageEventCompleted
    case admanMessageEventSkipped
}

let NOTIFICATION_ADMAN_PLAYER = "NotificationMessageEventAdmanPlayer"
let NOTIFICATION_ADMAN_PLAYER_KEY = "adman_player"
enum AdmanPlayerEventType : Int {
    case admanPlayerEventPrepare = 1
    case admanPlayerEventReady
    case admanPlayerEventFailed
    case admanPlayerEventPlaying
    case admanPlayerEventPlay
    case admanPlayerEventPause
    case admanPlayerEventProgress
    case admanPlayerEventComplete
    case admanPlayerEventCloseable
    case admanPlayerEventSkippable
}

let NOTIFICATION_ADMAN_VOICE = "NotificationMessageEventAdmanVoice"
let NOTIFICATION_ADMAN_VOICE_KEY = "adman_voice"
enum AdmanVoiceEventType : Int {
    case admanVoiceEventResponsePlaying = 1
    case admanVoiceEventInteractionStarted
    case admanVoiceEventInteractionEnded
    case admanVoiceEventError
    case admanVoiceEventRecognizerStarted
    case admanVoiceEventRecognizerStopped
}

let NOTIFICATION_ADMAN_CONTROL = "NotificationMessageEventAdmanControl"
let NOTIFICATION_ADMAN_CONTROL_KEY = "adman_control"
enum AdmanControlEventType : Int {
    case admanControlEventPrepare = 1
    case admanControlEventStart
    case admanControlEventPause
    case admanControlEventResume
    case admanControlEventSkip
    case admanControlEventClick
    case admanControlEventLink
    case admanControlEventSwipeBack
    case admanControlEventPositive
    case admanControlEventNegative
}

class NotificationAdmanBase: NSObject {
    var event: AdmanMessageEventType!

    class func sendEvent(_ event: AdmanMessageEventType) {
    }

    class func getEventString(_ event: AdmanMessageEventType) -> String {
    }
}

class NotificationAdmanPlayer: NSObject {
    private(set) var event: AdmanPlayerEventType!
    private(set) var userData: Any?

    init(_ event: AdmanPlayerEventType, andUserData userData: Any?) {
    }

    class func sendEvent(_ event: AdmanPlayerEventType) {
    }

    class func sendEvent(_ event: AdmanPlayerEventType, userData: Any?) {
    }

    class func getEventString(_ event: AdmanPlayerEventType) -> String {
    }
}

class NotificationAdmanVoice: NSObject {
    var event: AdmanVoiceEventType!

    class func sendEvent(_ event: AdmanVoiceEventType) {
    }

    class func getEventString(_ event: AdmanVoiceEventType) -> String {
    }
}

class NotificationAdmanControl: NSObject {
    var event: AdmanControlEventType!
    private(set) var userData: Any?

    init(_ event: AdmanControlEventType, andUserData userData: Any?) {
    }

    class func sendEvent(_ event: AdmanControlEventType) {
    }

    class func sendEvent(_ event: AdmanControlEventType, userData: Any?) {
    }

    class func getEventString(_ event: AdmanControlEventType) -> String {
    }
}
}
