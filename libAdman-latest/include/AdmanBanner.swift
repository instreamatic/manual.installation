/// Copyright 2021 Instreamatic
/// This program is free software: you can redistribute it and/or modify
/// it under the terms of the GNU Affero General Public License as published by
/// the Free Software Foundation, either version 3 of the License, or
/// (at your option) any later version.
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU Affero General Public License for more details.
/// You should have received a copy of the GNU Affero General Public License
/// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import AVFoundation
import Foundation
import UIKit

func AdmanOnFulfill(_ Named: Any) {
    let Named: ((Any?) -> Void)? = nil
}

enum AdmanPreloadMode : Int {
    case progressive
    case full
}

class AdmanMediaFile: NSObject {
    var bitrate = 0
    var type = ""
    var url = ""

    init(source: String, type: String, andBitrate bitrate: Int) {
    }
}

/// Voice action type for voice ad (audio, link, phone)
class AdmanResponseValue: NSObject {
    /// Value type
    var type = ""
    /// @property data
    var data: NSObject?

    init(type: String, andData data: String?) {
    }
}

class AdmanInteraction: NSObject {
    var priority = 0
    var action = ""
    /// @property type Possible types: positive, negative, silence, skip, unknown
    var type = ""
    /// @property values collection with core data for action after response
    var values: [AdmanResponseValue] = []
    /// @property keywords
    var keywords: [String] = []

    init(action: String, andType type: String) {
    }
}

/// Instreamatic creative
class AdmanBanner: NSObject {
    var adId: String?
    var adSystem: String?
    /// @property expirationTime Ad expiration time after preload request, in seconds
    /// Default value - 5 minuts
    var expirationTime = 0
    /// @property sourceUrl link to audio ad
    var sourceUrl: String?
    /// @property introUrl link to ad intro
    var introUrl: String?
    /// @property bitrate audio ad bitrate
    var bitrate = 0
    /// @property duration of audio ad
    var duration = 0
    /// @property minVolume audio volume cannot be lower then this value
    var minVolume = 0
    var adText: String?
    /// @property image url for banner
    var url: String?
    /// @property urlToNavigateOnClick link to open in internal browser
    var urlToNavigateOnClick: String?
    /// @property media collection of all media links
    var media: [AdmanMediaFile] = []
    /// Companion ads list
    /// key - string formatted as HxW (e.g. 640x640, 350x200)
    /// value - image url
    var companionAds: [String : AdmanCreative]?
    /// @property bannerImage ad image created for default size when ad show on device display
    var bannerImage: UIImage?
    /// @property adCache UIImage in binary format for default image size
    var adCache: Data?
    /// @property statistics<NSString*, NSArray*<NSString*>> collection of all root VAST stat events to report
    var statData: AdmanStats!
    /// @property responses collection for all voice interactions
    var responses: [String : [AdmanInteraction]]?
    /// Time until microphone will be disabled on voice interaction step
    var responseTime = 0
    var assets: [String : Any?] = [:]
    var showControls = false
    var isClickable = false

    func isVoiceAd() -> Bool {
    }
}
// type NSString *
let AdmanBannerAssetMicOnUrl = "micOnUrl"
let AdmanBannerAssetMicOffUrl = "micOffUrl"
// type AVAsset *
let AdmanBannerAssetMicOnCache = "micOnCache"
let AdmanBannerAssetMicOffCache = "micOffCache"
